import pickle
from imutils import paths
import numpy as np
import imutils
import cv2
import os

def mapping(path):
  le = pickle.loads(open(path, "rb").read())
  listStudents = le.classes_

  map = {}
  for student in listStudents:
    map[student] = 0
  map['unknown'] = 0
  return map

def recognizing(mapper, imagePaths):
  # paths
  a_dataset = 'test_dataset'
  recognizer = '../training/output/recognizer.pickle'
  le_pickle = '../training/output/le.pickle'
  cascade = '../training/face_detection_classifier/intel_frontal_face_classifier.xml'
  embedding_model = '../training/openface_nn4.small2.v1.t7'

  recognizer = pickle.loads(open(recognizer, "rb").read())
  le = pickle.loads(open(le_pickle, "rb").read())
  face_cascade = cv2.CascadeClassifier(cascade)
  embedder = cv2.dnn.readNetFromTorch(embedding_model)

  for (i, imagePath) in enumerate(imagePaths):
    imageCounter = i + 1
    image = cv2.imread(imagePath)
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

    faces = face_cascade.detectMultiScale(gray, 1.1, 5)
    i = 0
    box = faces[i]
    (fx, fy, fw, fh) = box.astype("int")

    face = image[fy:fy + fh, fx:fx + fw]
    (fH, fW) = face.shape[:2]

    #if fW < 20 or fH < 20:
    #continue
    faceBlob = cv2.dnn.blobFromImage(face, 1.0 / 255, (96, 96),
                                   (0, 0, 0), swapRB=True, crop=False)
    embedder.setInput(faceBlob)
    vec = embedder.forward()

    preds = recognizer.predict_proba(vec)[0]
    j = np.argmax(preds)
    prob = preds[j]
    label = le.classes_[j]

    if prob < 0.5:
        label = 'unknown'

    mapper[label] = mapper[label] + 1
    print(imageCounter, label, prob)

def testing(path):
  mapper = mapping('../training/output/le.pickle')
  imagePaths = list(paths.list_images(path))
  recognizing(mapper, imagePaths)

  print('-----------------------------------------')
  print('----------------Statistic----------------')
  print('-----------------------------------------')
  for student in mapper:
    print(student, mapper[student], '/ 50')

if __name__ == '__main__':
  student = 'TranPhuocGiaThuy-102170253'
  testing('test_dataset/' + 'OutOfClass/' + student)
