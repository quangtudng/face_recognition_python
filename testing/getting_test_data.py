import cv2, os, time
import RPi.GPIO as GPIO
import cv2, time, threading, os,pickle
import numpy as np
import glob
import shutil
from slugify import slugify
import RPi.GPIO as GPIO

def collect_data(username, userID, numberOfImages, isLightOn):
  path_faceCascade = cv2.CascadeClassifier(
    '../training/face_detection_classifier/intel_frontal_face_classifier.xml')

  if not os.path.exists('test_dataset/' + str(isLightOn) + '/' + username + "-" + userID):
    os.mkdir('test_dataset/' + str(isLightOn) + '/' + username + "-" + userID)
    
  GPIO.setmode(GPIO.BCM)
  GPIO.setup(18,GPIO.OUT)

  if isLightOn == True:
      GPIO.output(18, 1)  # light on

  count = 0
  cap = cv2.VideoCapture(0)

  cap.set(3, 640)  # width
  cap.set(4, 480)  # height
  while True:
    _, frame = cap.read()
    frame = cv2.flip(frame, -1)
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)  # Gray Frame (HAAR)
    faces = path_faceCascade.detectMultiScale(
      gray,
      scaleFactor=1.2,
      minNeighbors=5,
      minSize=(20, 20)
    )
    if len(faces) == 1:
      for (x, y, w, h) in faces:
        count = count + 1
        print(count)
        cv2.imwrite('test_dataset/' + str(isLightOn) + '/' + username +
                    "-" + userID + "/" + str(count) + ".jpg", frame)
        cv2.imwrite('test_dataset/' + str(isLightOn) + '/' + username +
                    "-" + userID + "/" + str(count) + ".jpg", frame)
    if count == numberOfImages:
      break
    cv2.waitKey(30)
    time.sleep(0.2)
  cap.release()  # Release hardware and software resources
  cv2.destroyAllWindows()  # Close all window
  #  Retrain and add new dataset

  if isLightOn == True:
      GPIO.output(18, 0)  # light off

if __name__ == '__main__':
  collect_data('NguyenQuangTu', '102170257', 50, False)

