from helper import *
attendance = Blueprint('attendance', __name__)
@attendance.route('/attendances')
def render_attendance():
  data = []
  dateQuery = None
  try:
    allUsers = db.child('users').child('17TCLC1').child('Students').get()
    countAttended = 0
    countAbsent = 0
    countPending = 0
    foundDate = False # To avoid invalid date
    # If date is not available then it will choose the current day
    if request.args.get('date', ''):
      dateQuery = request.args.get('date', '')
    else:
      dateQuery = datetime.date.today().strftime("%d-%m-%Y")
    if allUsers.val() is not None:
      for uniqueKey,value in allUsers.val().items():
        if value.get('checkup'):
        # Loop through list of keys instead of dictionary
          for key in list(value['checkup']):
            if key == dateQuery:
              # True = Attended
              if value['checkup'][key]['check'] == True:
                if value['checkup'][key].get('image') and value['checkup'][key].get('time'):
                  value['image'] = value['checkup'][key]['image']
                  value['time'] = value['checkup'][key]['time']
                  countAttended = countAttended + 1
                value['check'] = True
              # Pending = Awaiting for confirmation
              elif value['checkup'][key]['check'] == "Pending":
                if value['checkup'][key].get('image') and value['checkup'][key].get('time'):
                  value['image'] = value['checkup'][key]['image']
                  value['time'] = value['checkup'][key]['time']
                value['check'] = "Pending"
                countPending = countPending + 1
              # False = Absent
              elif value['checkup'][key]['check'] == False:
                if value['checkup'][key].get('image') and value['checkup'][key].get('time'):
                  value['image'] = value['checkup'][key]['image']
                  value['time'] = value['checkup'][key]['time']
                  value['time'] = value['checkup'][key]['time']
                value['check'] = False
                countAbsent = countAbsent + 1
              foundDate = True
          del value['checkup']
        value['key'] = uniqueKey
        # If data is found , add everything into an array
        if foundDate:
          data.append(value)
  except Exception as error:
    print(error)
  return render_template('./partials/getAttendance.html', results=data, dateQuery=dateQuery, countAbsent=countAbsent, countAttended=countAttended, countPending=countPending, server=server)
@attendance.route('/attendances/edit/<string:uniqueKey>', methods=['POST'])
@jwt_required
def edit_attendace(uniqueKey):
  try:
    if get_jwt_identity() == 'teacher':
      if request.form['selectedStatus'] == "True":
        status = True
      else:
        status = False
      db.child('users').child('17TCLC1').child('Students').child(uniqueKey).child('checkup').child(request.form['selectedDate']).update({
        'check': status
      })
      return jsonify({ 'status': 'ok' })
    else:
      return jsonify({ 'status': 'unauthorized' })
  except:
    return jsonify({ 'status': 'failed' })