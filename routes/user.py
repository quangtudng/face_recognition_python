from helper import *
from training import train
user = Blueprint('user', __name__)
@user.route('/user/add', methods=['POST'])
@jwt_required
def add_user():
  try:
    if get_jwt_identity() == 'teacher':
      allUsers = db.child('users').child('17TCLC1').child('Students').get()
      if allUsers.val() is not None:
        for key,value in allUsers.val().items():
          if value['id'] == request.form['id']:
            return jsonify({ 'status': 'duplicate'})
      db.child('users').child('17TCLC1').child('Students').push({
        "id": request.form['id'], 
        "name": request.form['username'] 
      })
      return jsonify({ 'status': 'ok'})
    else:
      return jsonify({ 'status': 'unauthorized' })
  except Exception as error:
    print(error)
    return jsonify({ 'status': 'failed' })
@user.route('/users') 
def read_users():
  data = []
  try:
    allUsers = db.child('users').child('17TCLC1').child('Students').get()
    if allUsers.val() is not None:
      for key,value in allUsers.val().items():
        value['key'] = key
        data.append(value)
  except Exception as error:
    print(error)
  return render_template('./partials/userTable.html', results=data, server=server)
@user.route('/user/delete/<string:uniqueKey>', methods=['POST'])
@jwt_required
def delete_user(uniqueKey):
  try:
    if get_jwt_identity() == 'teacher':
      # Delete dataset
      allUsers = db.child('users').child('17TCLC1').child('Students').get()
      if allUsers.val() is not None:
        for key,value in allUsers.val().items():
          if key == uniqueKey:
            usernameSlug = slugify(value['name'],separator='',lowercase=False)
            userIDSlug = slugify(value['id'],separator='',lowercase=False)

            remove_from_embeddings(usernameSlug, userIDSlug)
      # Delete firebase
      db.child('users').child('17TCLC1').child('Students').child(uniqueKey).remove()
      return jsonify({ 'status': 'ok' })
    else:
      return jsonify({ 'status': 'unauthorized' })
  except Exception as error:
    return jsonify({ 'status': 'failed' })

# supporter paths
address_knownNames = './training/output/knownNames.pickle'
address_knownEmbeddings = './training/output/knownEmbeddings.pickle'
address_embeddings = './training/output/embeddings.pickle'

# inner function for removing user
def remove_from_embeddings(usernameSlug, userIDSlug):
  user = usernameSlug + '-' + userIDSlug
  knownNames = pickle.loads(open(address_knownNames, "rb").read())
  knownEmbeddings = pickle.loads(open(address_knownEmbeddings, "rb").read())

  indexes = []
  for i, name in enumerate(knownNames):
    if name == user:
      indexes.append(i)
  try:
    del knownNames[indexes[0]:indexes[len(indexes) - 1]]
    del knownEmbeddings[indexes[0]:indexes[len(indexes) - 1]]

    # delete the last face, embedding
    if user in knownNames:
      for i, name in enumerate(knownNames):
        if name == user:
          del knownNames[i]
          del knownEmbeddings[i]
          break

    shutil.rmtree('./training/dataset/' + user)

    data = {"embeddings": knownEmbeddings, "names": knownNames}
    f = open(address_embeddings, "wb")
    f.write(pickle.dumps(data))
    f = open(address_knownNames, "wb")
    f.write(pickle.dumps(knownNames))
    f = open(address_knownEmbeddings, "wb")
    f.write(pickle.dumps(knownEmbeddings))
    f.close()
    train.train()
    print('[Delete From User] Successfully')
  except:
    print('[Delete From User] The embedding of that user not exist')