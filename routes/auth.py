from helper import *
auth = Blueprint('auth', __name__)
@auth.route('/auth/login', methods=['POST'])
def login():
  try:
    username = request.form['username']
    password = request.form['password']
    print(username, password)
    if password == '123456' and username == 'teacher':
      access_token = create_access_token(identity=username)
      return jsonify({ 'status': 'ok' , 'token': access_token})
    else:
      return jsonify({ 'status': 'wrong' })
  except:
    return jsonify({ 'status' : 'failed' })