from helper import *
from training import *
from training import extract_new_embedding
data = Blueprint('data',__name__)
#! Collect data routes
@data.route('/dataset')
def render_collectData():
  return render_template('./partials/getData.html', server=server)
@data.route('/collect-data', methods=['POST'])
@jwt_required
def collectData():
  try:
    if get_jwt_identity() == 'teacher':
      usernameSlug = slugify(request.form['username'],separator='',lowercase=False)
      userIDSlug = slugify(request.form['userID'],separator='',lowercase=False)
      # Create new user in Firebase if ID not exists
      # Use the name from Firebase if ID exists
      allUsers = db.child('users').child('17TCLC1').child('Students').get()
      foundUser = False
      if allUsers.val() is not None:
        for key,value in allUsers.val().items():
          if value['id'] == request.form['userID']:
            usernameSlug = slugify(value['name'],separator='',lowercase=False)
            foundUser = True
      if not foundUser:
        db.child('users').child('17TCLC1').child('Students').push({
          "id": request.form['userID'], 
          "name": request.form['username']
        })
      if not os.path.exists('./training/dataset/' + usernameSlug + "-" + userIDSlug):
        os.mkdir('./training/dataset/' + usernameSlug + "-" + userIDSlug)
      if not os.path.exists('./training/temporary_dataset/' + usernameSlug + "-" + userIDSlug):
        os.mkdir('./training/temporary_dataset/' + usernameSlug + "-" + userIDSlug)
      count = 0
      cap = cv2.VideoCapture(0)
      GPIO.output(18,1) # light on
      cap.set(3,640) # width
      cap.set(4,480) # height
      while True:
        _, frame = cap.read()
        frame = cv2.flip(frame, -1)
        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY) # Gray Frame (HAAR)
        faces = path_faceCascade.detectMultiScale(
            gray,     
            scaleFactor=1.2,
            minNeighbors=5,
            minSize=(20,20)
        )
        if len(faces) == 1:
          for (x,y,w,h) in faces:
            count = count + 1
            print(count)
            cv2.imwrite('./training/dataset/' + usernameSlug +
                        "-" + userIDSlug + "/" + str(count) + ".jpg", frame)
            cv2.imwrite('./training/temporary_dataset/' + usernameSlug +
                        "-" + userIDSlug + "/" + str(count) + ".jpg", frame)
        if count == 50:
          break
        cv2.waitKey(30)
        time.sleep(0.2)
      cap.release()  # Release hardware and software resources
      cv2.destroyAllWindows() # Close all window
      #  Retrain and add new dataset
      GPIO.output(18,0) # light off
      return jsonify({'status': 'ok'})
    else:
      return jsonify({ 'status': 'unauthorized' })
  except Exception as error:
    print(error)
    usernameSlug = slugify(request.form['username'],separator='',lowercase=False)
    userIDSlug = slugify(request.form['userID'],separator='',lowercase=False)
    # If exception then delete the folder
    if os.path.exists('./training/dataset/' + usernameSlug + "-" + userIDSlug):
      shutil.rmtree('./training/dataset/' + usernameSlug + "-" + userIDSlug)
    if os.path.exists('./training/temporary_dataset/' + usernameSlug + "-" + userIDSlug):
      shutil.rmtree('./training/temporary_dataset/' + usernameSlug + "-" + userIDSlug)
    return jsonify({'status': 'failed'})
@data.route('/train-data', methods=['POST'])
def train_data():
  try:
    extract_new_embedding.embeddings_new_ds()
    # Delete temp file
    usernameSlug = slugify(request.form['username'],separator='',lowercase=False)
    userIDSlug = slugify(request.form['userID'],separator='',lowercase=False)
    allUsers = db.child('users').child('17TCLC1').child('Students').get()
    foundUser = False
    if allUsers.val() is not None:
      for key,value in allUsers.val().items():
        if value['id'] == request.form['userID']:
          usernameSlug = slugify(value['name'],separator='',lowercase=False)
    if os.path.exists('./training/temporary_dataset/' + usernameSlug + "-" + userIDSlug):
      shutil.rmtree('./training/temporary_dataset/' + usernameSlug + "-" + userIDSlug)
    return jsonify({'status': 'ok'})
  except Exception as error:
    print(error)
    usernameSlug = slugify(request.form['username'],separator='',lowercase=False)
    userIDSlug = slugify(request.form['userID'],separator='',lowercase=False)
    # If exception then delete the folder
    if os.path.exists('./training/dataset/' + usernameSlug + "-" + userIDSlug):
      shutil.rmtree('./training/dataset/' + usernameSlug + "-" + userIDSlug)
    if os.path.exists('./training/temporary_dataset/' + usernameSlug + "-" + userIDSlug):
      shutil.rmtree('./training/temporary_dataset/' + usernameSlug + "-" + userIDSlug)
    return jsonify({'status': 'failed'})
