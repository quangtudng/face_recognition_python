from helper import *
checkup = Blueprint('checkup', __name__)
@checkup.route('/')
def index():
  data = []
  allUsers = db.child('users').child('17TCLC1').child('Students').get()
  if allUsers.val() is not None:
    for key,value in allUsers.val().items():
      data.append(value)
  return render_template('./partials/index.html', allUsers=data, server=server)
@checkup.route('/start-check-up', methods=['POST'])
def startCheckUp():
  try:
    currentTime = datetime.datetime.now().strftime("%H:%M:%S")
    currentTimeObject = datetime.datetime.now().time()
    # Check time of checkup
    if not (currentTimeObject <= datetime.time(23, 0, 0) and currentTimeObject >= datetime.time(6, 0, 0)):
      return jsonify({'status': 'notInTime'})
    embedder = cv2.dnn.readNetFromTorch(path_embedding_model)
    # Face recognizer model
    recognizer = pickle.loads(open(path_recognizer, "rb").read())
    le = pickle.loads(open(path_le_pickle, "rb").read())

    cap = cv2.VideoCapture(0)
    GPIO.output(18,1)
    cap.set(3,640) 
    cap.set(4,480)
    ret, frame = cap.read()
    frame = cv2.flip(frame, -1)
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    faces = path_faceCascade.detectMultiScale(
        gray,     
        scaleFactor=1.2,
        minNeighbors=5,
        minSize=(20,20)
    )
    if len(faces) > 0:
      for i in range(0, len(faces)):
        # extract the confidence (i.e., probability) associated with the
        box = faces[i]
        (fx, fy, fw, fh) = box.astype("int")
        # extract the face ROI
        face = frame[fy:fy + fh, fx:fx + fw]
        (fH, fW) = face.shape[:2]
        # ensure the face width and height are sufficiently large
        if fW < 20 or fH < 20:
          continue
        # construct a blob for the face ROI, then pass the blob
        # through our face embedding model to obtain the 128-d
        # quantification of the face
        faceBlob = cv2.dnn.blobFromImage(face, 1.0 / 255, (96, 96), (0, 0, 0), swapRB=True, crop=False)
        embedder.setInput(faceBlob)
        vec = embedder.forward()
        # perform classification to recognize the face
        preds = recognizer.predict_proba(vec)[0]
        j = np.argmax(preds)
        prob = preds[j]
        name = le.classes_[j]
        if prob >= 0.4:
          id = name.split('-')[1]
          name = name.split('-')[0]
        else:
          id = 0
          name = 'unknown'
        attendanceID = id
        attendanceName = name
        print(id, ' ',name, ' ', prob * 100)
        cv2.rectangle(frame, (fx, fy), (fx + fw, fy + fh), (255, 0, 0), 2)
    # No face
    if(len(faces) == 0):
      attendanceID = "unknown"
      attendanceName="unknown"
    cv2.imwrite('./static/img/last_taken/lastImg.jpg', frame)
    # More than 1 face
    if len(faces) > 1:
      GPIO.output(18, 0)
      return jsonify({'status': 'duplicate'})
    cap.release()
    cv2.destroyAllWindows()
    GPIO.output(18,0)
    return jsonify({'id': attendanceID, 'name': attendanceName, 'status': 'ok'})
  except Exception as error:
    print(error)
    GPIO.output(18,0)
    return jsonify({'status': 'failed'})
@checkup.route('/confirm', methods=['POST'])
def confirm_checkup():
  try:
    update_database(True,request.form['id'])
    return jsonify({'status': 'ok'})
  except:
    return jsonify({'status': 'failed'})
@checkup.route('/confirm-pending', methods=['POST'])
def confirm_checkup_pending():
  try:
    update_database("Pending",request.form['id'])
    return jsonify({'status': 'ok'})
  except:
    return jsonify({'status': 'failed'})
def update_database(status,id):
  currentDate = datetime.date.today().strftime("%d-%m-%Y")
  currentTime = datetime.datetime.now().strftime("%H:%M:%S")
  # Copying last image taken to attendance image folder
  imageFolder = './static/img/attendance_images/' + id + '/' + currentDate
  if not os.path.exists(imageFolder):
    os.makedirs(imageFolder)
  frame = cv2.imread('./static/img/last_taken/lastImg.jpg')
  cv2.imwrite(imageFolder + '/' + currentDate + '-' + id + '.jpg', frame)
  # Update image path
  imageFolder = imageFolder + '/' + currentDate + '-' + id + '.jpg'
  # Push information to database
  allUsers = db.child('users').child('17TCLC1').child('Students').get()
  if allUsers.val() is not None:
    for key,value in allUsers.val().items():
      if(str(value['id']) == id):
        # Update status if true
        db.child('users').child('17TCLC1').child('Students').child(key).child('checkup').child(currentDate).update({ 'check': status , 'image': imageFolder, 'time': currentTime })
      else:
        # Update false if not true and None
        if(db.child('users').child('17TCLC1').child('Students').child(key).child('checkup').child(currentDate).get().val() is None):
          db.child('users').child('17TCLC1').child('Students').child(key).child('checkup').child(currentDate).update({ 'check': False })