from helper import *
from routes.checkup import checkup
from routes.user import user
from routes.data import data
from routes.attendance import attendance
from routes.auth import auth
#! After request and before response
@app.after_request
def add_header(response):
  response.headers['X-UA-Compatible'] = 'IE=Edge,chrome=1'
  response.headers['Cache-Control'] = 'public, max-age=0, must-revalidate, no-store'
  return response
app.register_blueprint(auth)
app.register_blueprint(checkup)
app.register_blueprint(data)
app.register_blueprint(attendance)
app.register_blueprint(user)
if __name__ == '__main__':
  app.run(host=server,use_reloader=False, debug=True, threaded=True)